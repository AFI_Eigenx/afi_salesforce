/******************************************************************************
* Class: WebCaseRequestApexController
-------------------------------------------------------------------------------
* Purpose/Methods:
* - Purpose: This controller will be used by WebCaseRequest lightning pages. 
- If email address entered on the form is associated with a Person Account, 
associate Case to that account. If no Person Account is found, create a new 
Person Account, and associate Case to that account. Also, if there is an 
attachment, attach it to the Case.
-------------------------------------------------------------------------------
* Unit Test: WebCaseRequestApexController_Test
-------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME      DATE            DETAIL FEATURES
	1.0      hmuppala(EigenX)    07/19/2017      INITIAL DEVELOPMENT
******************************************************************************/
public without sharing class WebCaseRequestApexController 
{
    private static final string  SUCCESS_MESSAGE = 'Record created successfully';
    private static final string  ERROR_MESSAGE = 'Error Message From Apex: n';
    // Get RecordTypes 
    static map<String, Id> accountRecordTypes = Utilities.GetRecordTypeIdsByDeveloperName(Account.SObjectType);
    static map<String, Id> caseRecordTypes    = Utilities.GetRecordTypeIdsByDeveloperName(Case.SObjectType);
    //Get RecordType Ids  
    static Id perRecTypeId = accountRecordTypes.get('PersonAccount');
    static Id afpRecTypeId = caseRecordTypes.get('AFP');
    public static Case myNewCase;
    
    // Get PickList values for the form fields
    @AuraEnabled
    public static List<String> getSelectOptionsDB(sObject objName, String fld)
    {
system.debug('> WCRAC.getSelectOptionsDB()');
system.debug('* objName: '+ objName);
system.debug('* fld: '+ fld);
        List<String> allOptions                   = new List<String>();
        Schema.SObjectType objType                = objName.getSobjectType();
        Schema.DescribeSObjectResult objDescribe  = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        List<Schema.PicklistEntry> pickListValues = fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pickListValues)
        {
            allOptions.add(a.getValue());
        }
        allOptions.sort();
system.debug('allOptions: '+ allOptions);
system.debug('< WCRAC.getSelectOptionsDB()');
        return allOptions;
    }

    // Translate form fields and picklist values based on the language selected by user
    @AuraEnabled
    public static Map<String, String> getTranslation(String language)
    {
system.debug('> WCRAC.getTranslation()');
system.debug('* language: '+ language);

        Map<String, String> englishToTransValueMap = new Map<String, String>();
        
        for (CaseFormTranslation__mdt conv : 
            [
                SELECT 
                    Form_Field__c, Translation__c 
                FROM 
                    CaseFormTranslation__mdt
                WHERE
                    Language__c = :language
            ])
        {
            englishToTransValueMap.put(conv.Form_Field__c, conv.Translation__c);
        }
        
system.debug('* englishToTransValueMap: '+ englishToTransValueMap);
system.debug('< WCRAC.getTranslation()');
        return englishToTransValueMap;
    }

    // Get Case number to display User after the case is successfully created.
    @AuraEnabled
    public static List<Case> getCaseNumber(Id caseId)
    {
system.debug('> WCRAC.getCaseNumber()');
system.debug('caseId: ' + caseId);
        List<Case> caseNumber = [SELECT CaseNumber FROM Case WHERE Id = :caseId];
system.debug('caseNumber: ' + caseNumber);
system.debug('< WCRAC.getCaseNumber()');
        return caseNumber;
    }

    // Residential Contact Us form
    @AuraEnabled
    public static Case createContactUsCaseDB(Account acct, Case myCase, String language)
    {
system.debug('> WCRAC.createContactUsCaseDB()');
system.debug('* Acct: '+ acct);
system.debug('* myCase: '+ myCase);
system.debug('* language: ' + language);

        AuraProcessingMessage returnMessage = new AuraProcessingMessage();        
        Case newCase                        = new Case();
        List<Account> caseAcct              = getPersonAccountDB(acct, language);
        
        if(!caseAcct.isEmpty())
        {
            Id acctId = caseAcct.get(0).Id;
            Map<String, String> floorTypeToProdUsageMap = getMappingValues('floorType', language);
            Map<String, String> claimChoiceToClaimFiled = getMappingValues('claimChoice', language);
            Map<String, String> subjectToAreaMap        = new Map<String, String>();
            Map<String, String> subjectToCategoryMap    = new Map<String, String>();
            
            // Map the Case form Picklist values with Salesforce Picklist values
            for (CaseFormConversion_Subject__mdt conv : 
                 [SELECT Area__c, Category__c, Subject__c, Spanish__c, French__c FROM CaseFormConversion_Subject__mdt]
                )
                if(language == 'Spanish')
            	{
                	subjectToAreaMap.put(conv.Spanish__c, conv.Area__c);
                	subjectToCategoryMap.put(conv.Spanish__c, conv.Category__c);
                } 
            	else if(language == 'French')
            	{
                	subjectToAreaMap.put(conv.French__c, conv.Area__c);
                	subjectToCategoryMap.put(conv.French__c, conv.Category__c);
                }
            	else
            	{
                	subjectToAreaMap.put(conv.Subject__c, conv.Area__c);
                	subjectToCategoryMap.put(conv.Subject__c, conv.Category__c);
                }

            newCase                         = myCase;
            newCase.Origin                  = 'Web';
            newCase.Status                  = 'New';
            newCase.RecordTypeId            = afpRecTypeId;
            newCase.AccountId               = acctId;
            newCase.Product_Usage__c        = floorTypeToProdUsageMap.get(myCase.Product_Usage__c);
            newCase.Consumer_Filed_Claim__c = claimChoiceToClaimFiled.get(myCase.Consumer_Filed_Claim__c);
            newCase.Area__c                 = subjectToAreaMap.get(myCase.Subject);
            newCase.Category__c             = subjectToCategoryMap.get(myCase.Subject);
system.debug('* newCase: '+ newCase);
            try
            {
                insert newCase;
                newCase = (case)getCaseRecord(newCase.Id);
                returnMessage.successMsg=SUCCESS_MESSAGE;
            }
            catch (Exception e)
            {
system.debug('The following exception occurred: '+ e.getMessage());
                returnMessage.isSuccess = false;
                returnMessage.errorMsg = ERROR_MESSAGE + e.getMessage();
            }
        }
system.debug('< WCRAC.createContactUsCaseDB()');
        return newCase;
    }
 
    // Product Registration form
    @AuraEnabled
    public static Case createProdRegCaseDB(Account acct, Case myCase, String language)
    {
system.debug('> WCRAC.createProdRegCaseDB()');
system.debug('* Acct: '+ acct);
system.debug('* myCase: '+ myCase);
system.debug('* language: ' + language);
        
        AuraProcessingMessage returnMessage = new AuraProcessingMessage(); 
        Case newCase                        = new Case();
        List<Account> caseAcct              = getPersonAccountDB(acct, language);
        
        if (!caseAcct.isEmpty())
        {
            Id acctId = caseAcct.get(0).Id;
            Map<String, String> floorTypeToProdUsageMap = getMappingValues('floorType', language);
            Map<String, String> hearAboutUs             = getMappingValues('hearAboutUs', language);
            Map<String, String> reasonPurchased         = getMappingValues('reasonPurchased', language);

            newCase                     = myCase;
            newCase.Origin              = 'Web';
            newCase.Status              = 'New';
            newCase.RecordTypeId        = afpRecTypeId;
            newCase.AccountId           = acctId;
            newCase.Product_Usage__c    = floorTypeToProdUsageMap.get(myCase.Product_Usage__c);
            newCase.Hear_About_Us__c    = hearAboutUs.get(myCase.Hear_About_Us__c);
            newCase.Reason_Purchased__c = reasonPurchased.get(myCase.Reason_Purchased__c);
system.debug('* newCase: '+ newCase);
            try
            {
                insert newCase;
                newCase = (case)getCaseRecord(newCase.Id);
                returnMessage.successMsg=SUCCESS_MESSAGE;
            }
            catch (Exception e)
            {
system.debug('The following exception occurred: '+ e.getMessage());
                returnMessage.isSuccess = false;
                returnMessage.errorMsg = ERROR_MESSAGE + e.getMessage();
            }
        }
system.debug('< WCRAC.createProdRegCaseDB()');
        return newCase;
    }
    
    // Get Person Account based on the Email entered on the form
    public static List<Account> getPersonAccountDB(Account acct, String language)
    {
system.debug('> WCRAC.getPersonAccountDB()');
system.debug('* acct: ' + acct);
        
        Map<String, String> roleToTypeMap          = getMappingValues('role', language);
        Map<String, String> contactToContactMethod = getMappingValues('preferredContact', language);
        List<Account> myPerAcct                    = new List<Account>();

        List<Account> perAccList = 
            [
                SELECT 
                    Id, Name, PersonEmail
                FROM 
                    Account
                WHERE
                    RecordTypeId = :perRecTypeId
                    AND
                    PersonEmail = :acct.PersonEmail
            ];
system.debug('* perAccList.Size(): '+ perAccList.size());
system.debug('* perAccList: '+ perAccList);

        // If more than one Person account exists for the email entered, query based on Name and Email.
        // If no record exists, create a Person account. 
        if (perAccList.size() == 1)
        {
            myPerAcct.addAll(perAccList);
        }
        else if (perAccList.size() > 1)
        {
            List <Account> newPerAccList = 
                [
                    SELECT 
                        Id, Name, PersonEmail
                    FROM 
                        Account
                    WHERE
                        RecordTypeId = :perRecTypeId
                        AND
                        PersonEmail = :acct.PersonEmail
                        AND
                        FirstName = :acct.FirstName
                        AND
                        LastName = :acct.LastName
                ];
            if (newPerAccList.isEmpty()) 
            {
                myPerAcct.addAll(perAccList);
            }
            else 
            {
                myPerAcct.addAll(newPerAccList);
            }
        }
        else
        {
            Account newAcct                     = acct;
            newAcct.Type                        = roleToTypeMap.get(acct.Type);
            newAcct.Preferred_Contact_Method__c = contactToContactMethod.get(acct.Preferred_Contact_Method__c);
            newAcct.RecordTypeId                = perRecTypeId;
            try
            {
                insert newAcct;
                myPerAcct.add(newAcct);
            }
            catch (Exception e)
            {
                system.debug('The following exception occurred: '+ e.getMessage());
            }
        }
system.debug('* Inserted myPerAcct: '+ myPerAcct);
system.debug('< WCRAC.getPersonAccountDB()');
        return myPerAcct;
    }
    
    // Upload Attachment
    @AuraEnabled
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) 
    {
system.debug('> WCRAC.saveTheFile()');
system.debug('parentId: ' + parentId);
system.debug('fileName: ' + fileName);
system.debug('base64Data: ' + base64Data);
system.debug('contentType: ' + contentType);
        base64Data         = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment attach  = new Attachment();
        attach.ParentId    = parentId;
        attach.Name        = fileName;
        attach.Body        = EncodingUtil.base64Decode(base64Data);
        attach.ContentType = contentType;
        insert attach;
system.debug('Attachment Id: ' + attach.Id);
system.debug('< WCRAC.saveTheFile()');
        return attach.Id;
    }
    
    @AuraEnabled
    public static Id saveTheChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) 
    {
system.debug('> WCRAC.saveTheChunk()');
system.debug('parentId: ' + parentId);
system.debug('fileName: ' + fileName);
system.debug('base64Data: ' + base64Data);
system.debug('contentType: ' + contentType);
system.debug('fileId: ' + fileId);
        if (fileId == '') 
        {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } 
        else 
        {
            appendToFile(fileId, base64Data);
        }
system.debug('< WCRAC.saveTheChunk()');
system.debug('Id.valueOf(fileId): ' + Id.valueOf(fileId));
        return Id.valueOf(fileId);
    }
    
    private static void appendToFile(Id fileId, String base64Data) 
    { 
system.debug('> WCRAC.appendToFile()');
system.debug('fileId: ' + fileId);
system.debug('base64Data: ' + base64Data);

        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
system.debug('base64Data appended: ' + base64Data);
        
        Attachment attach = [SELECT Id, Body FROM Attachment WHERE Id = :fileId];
     	
        String existingBody = EncodingUtil.base64Encode(attach.Body);
        attach.Body         = EncodingUtil.base64Decode(existingBody + base64Data); 
        update attach;
system.debug('Attachment Chunk: ' + attach);
system.debug('< WCRAC.appendToFile()');
    }

    // Get Mapping values from Custom Metadata Types
    public static Map<String, String> getMappingValues(String convert, String language)
    {
system.debug('> WCRAC.getMappingValues()');
system.debug('* convert: '+ convert);
system.debug('language in getMappingValues: ' + language);

        Map<String, String> formFldToObjectFldMap = new Map<String, String>();
        
        if (convert == 'role')
        {
            for (CaseFormConversion_Role__mdt conv : 
                    [SELECT Type__c, Role__c, Spanish__c, French__c FROM CaseFormConversion_Role__mdt]
                )
                if(language == 'Spanish'){
                    formFldToObjectFldMap.put(conv.Spanish__c, conv.Type__c);
                }
            	else if(language == 'French'){
                	formFldToObjectFldMap.put(conv.French__c, conv.Type__c);
                }else{
                	formFldToObjectFldMap.put(conv.Role__c, conv.Type__c);
                }
        }
        else if(convert == 'floorType')
        {
            for (CaseFormConversion_FloorType__mdt conv : 
                 [SELECT Floor_Type__c, Product_Usage__c, Spanish__c, French__c FROM CaseFormConversion_FloorType__mdt]
                )
                if(language == 'Spanish'){
                	formFldToObjectFldMap.put(conv.Spanish__c, conv.Product_Usage__c);
                }
            	else if(language == 'French'){
                	formFldToObjectFldMap.put(conv.French__c, conv.Product_Usage__c);
                } else{
                	formFldToObjectFldMap.put(conv.Floor_Type__c, conv.Product_Usage__c);
                }
        }
        else if(convert == 'preferredContact')
        {
        	for (CaseFormConversion_PreferredContact__mdt conv : 
                 [SELECT Preferred_Contact__c, Preferred_Contact_Method__c, Spanish__c, French__c FROM CaseFormConversion_PreferredContact__mdt]
                )
                if(language == 'Spanish'){
                	formFldToObjectFldMap.put(conv.Spanish__c, conv.Preferred_Contact_Method__c);
                }
            	else if(language == 'French'){
                	formFldToObjectFldMap.put(conv.French__c, conv.Preferred_Contact_Method__c);
                } else{
                	formFldToObjectFldMap.put(conv.Preferred_Contact__c, conv.Preferred_Contact_Method__c);
                }
        }
		else if(convert == 'claimChoice')
        {
        	for (CaseFormConversion_ClaimChoice__mdt conv : 
                 [SELECT Consumer_Filed_Claim__c, Claim_Filed__c, Spanish__c, French__c FROM CaseFormConversion_ClaimChoice__mdt]
                )
                if(language == 'Spanish'){
                	formFldToObjectFldMap.put(conv.Spanish__c, conv.Consumer_Filed_Claim__c);
                }
            	else if(language == 'French'){
                	formFldToObjectFldMap.put(conv.French__c, conv.Consumer_Filed_Claim__c);
                } else{
                	formFldToObjectFldMap.put(conv.Claim_Filed__c, conv.Consumer_Filed_Claim__c);
                }
        }
        else if(convert == 'hearAboutUs')
        {
            for (ProductRegForm_HearAboutUs__mdt conv : 
                 [SELECT Hear_About_Armstrong__c, Spanish__c, French__c FROM ProductRegForm_HearAboutUs__mdt]
                )
                if(language == 'Spanish'){
                	formFldToObjectFldMap.put(conv.Spanish__c, conv.Hear_About_Armstrong__c);
                }
            	else if(language == 'French'){
                	formFldToObjectFldMap.put(conv.French__c, conv.Hear_About_Armstrong__c);
                } else{
                	formFldToObjectFldMap.put(conv.Hear_About_Armstrong__c, conv.Hear_About_Armstrong__c);
                }
        }
        else if(convert == 'reasonPurchased')
        {
           for (ProductRegForm_ReasonPurchased__mdt conv : 
                 [SELECT Purchased_Reason__c, Spanish__c, French__c FROM ProductRegForm_ReasonPurchased__mdt]
                )
                if(language == 'Spanish'){
                	formFldToObjectFldMap.put(conv.Spanish__c, conv.Purchased_Reason__c);
                }
            	else if(language == 'French'){
                	formFldToObjectFldMap.put(conv.French__c, conv.Purchased_Reason__c);
                } else{
                	formFldToObjectFldMap.put(conv.Purchased_Reason__c, conv.Purchased_Reason__c);
                } 
        }
system.debug('* formFldToObjectFldMap: '+ formFldToObjectFldMap);
system.debug('< WCRAC.getMappingValues()');
        return formFldToObjectFldMap;
    }


    public static sObject getCaseRecord(Id myId)
    {
system.debug('> WCRAC.getCaseRecord()');
system.debug('* myId: '+ myId);

        sObject myCaseRecord = new Case();
        String myQuery;
        Schema.DescribeSObjectResult describeResult = Case.getSobjectType().getDescribe();
        List<String> fieldNames = new List<String>(describeResult.fields.getMap().keySet()); 

        myQuery = 'SELECT '+ String.join(fieldNames, ',')+ ' FROM '+ describeResult.getName()+ ' WHERE '+ 'Id = :myId';

        myCaseRecord = Database.query(myQuery);
system.debug('< WCRAC.getCaseRecord()');
        return myCaseRecord;
    }
}